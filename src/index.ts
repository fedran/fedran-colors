import * as Color from "color";

export enum ColorType {
    primary = "primary",
    contrast = "contrast",
}

export enum ColorIntensity {
    base = 50,
    lightish = 60,
    light = 70,
    lighter = 80,
    lightest = 90,
    darkish = 40,
    dark = 30,
    darker = 20,
    darkest = 10,
}

export interface ColorCallback {
    (type: ColorType, intensity: ColorIntensity, color: Color): void;
}

export class ColorSet {
    public base: Color;
    public type: ColorType;

    constructor(type: ColorType, base: Color) {
        this.type = type;
        this.base = base;
    }

    public get lightish(): Color {
        return this.base.lightness(60);
    }

    public get light(): Color {
        return this.base.lightness(70);
    }

    public get lighter(): Color {
        return this.base.lightness(80);
    }

    public get lightest(): Color {
        return this.base.lightness(90);
    }

    public get darkish(): Color {
        return this.base.lightness(40);
    }

    public get dark(): Color {
        return this.base.lightness(30);
    }

    public get darker(): Color {
        return this.base.lightness(20);
    }

    public get darkest(): Color {
        return this.base.lightness(10);
    }

    /**
     * Gets the base color modified by lighteness.
     */
    public lightness(value: number): Color {
        return this.base.lightness(value);
    }

    public forEach(callback: ColorCallback) {
        callback(this.type, ColorIntensity.lightest, this.lightest);
        callback(this.type, ColorIntensity.lighter, this.lighter);
        callback(this.type, ColorIntensity.light, this.light);
        callback(this.type, ColorIntensity.lightish, this.lightish);

        callback(this.type, ColorIntensity.base, this.base);

        callback(this.type, ColorIntensity.darkish, this.darkish);
        callback(this.type, ColorIntensity.dark, this.dark);
        callback(this.type, ColorIntensity.darker, this.darker);
        callback(this.type, ColorIntensity.darkest, this.darkest);
    }
}

export class ColorScheme {
    constructor(specification: string) {
        // Parse the primary color, then use rotation to get the other one.
        const primaryColor = Color(specification).lch().lightness(50);
        const secondaryColor = primaryColor.hue(primaryColor.hue() + 180);

        this.primary = new ColorSet(ColorType.primary, primaryColor);
        this.secondary = new ColorSet(ColorType.contrast, secondaryColor);
    }

    public primary: ColorSet;
    public secondary: ColorSet;

    public forEach(callback: ColorCallback) {
        this.primary.forEach(callback);
        this.secondary.forEach(callback);
    }
}
