import * as Color from "color";
import { ColorScheme } from "../index";

export const command = "single-gpl-palette <base-color>";
export const describe =
    "builds out the Gimp GPL palette for a single base color";

export const builder = {
    title: {
        describe: "The title for the color palette",
    },
};

function toPaletteEntry(lightness: number, lch: Color, title: string): string {
    // Take the LCH color and convert it into RGB.
    const rgb = lch.rgb();
    const [r, g, b] = rgb.rgb().array();

    // Inkscape can't handle decimal numbers, so strip them off.
    const nr = ("   " + parseInt(r.toString())).slice(-3);
    const ng = ("   " + parseInt(g.toString())).slice(-3);
    const nb = ("   " + parseInt(b.toString())).slice(-3);

    // Write out the results.
    return `${nr} ${ng} ${nb} ${title}-${lightness}\n`;
}

export function handler(argv: { baseColor: string; title: string }) {
    // Write out the HTML for the sample.
    const title = argv.title ? " - " + argv.title : "";
    let buffer = `GIMP Palette
Name: Fedran${title}
Columns: 18

`;

    // Write out the colors. We don't bother with lightenst
    const scheme = new ColorScheme(argv.baseColor);

    [10, 20, 30, 40, 50, 60, 70, 80, 90].forEach((x) => {
        buffer += toPaletteEntry(x, scheme.primary.lightness(x), "primary");
    });

    [10, 20, 30, 40, 50, 60, 70, 80, 90].forEach((x) => {
        buffer += toPaletteEntry(x, scheme.secondary.lightness(x), "secondary");
    });

    // Write out the file to the console.
    console.log(buffer);
}
