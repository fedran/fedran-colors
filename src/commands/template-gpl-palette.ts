import * as singleGplPalette from "./single-gpl-palette";

export const command = "template-gpl-palette";
export const describe = "builds out a standard GPL palette for template covers";

export const builder = {};

export function handler() {
    // This seems to be a nice color to work with.
    singleGplPalette.handler({
        title: "Template Colors",
        baseColor: "#007fc1",
    });
}
