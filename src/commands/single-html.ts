import { ColorScheme } from "../index";

export const command = "single-html <base-color>";
export const describe = "builds out the color scheme for a single base color";

export const builder = {};

export function handler(argv: { baseColor: string }) {
    // Get the color scheme for the input.
    const scheme = new ColorScheme(argv.baseColor);

    // Write out the HTML for the sample.
    let buffer = `<html>
  <head>
    <title>Color Sample</title>
    <style>
    .box {
        height: 100px;
        width: 100px;
        display: inline-block;
        margin: 5px;
    }
`;

    scheme.forEach((type, intensity, color) => {
        buffer += `
        #color-${type}-${intensity} {
            background-color: ${color.toString()};
        }
        `;
    });

    buffer += `
    </style>
  </head>
  <body>
    <div>
`;

    scheme.forEach((type, intensity) => {
        buffer += `<div class='box' id='color-${type}-${intensity}'></div>`;
    });

    buffer += `</body></html>`;

    console.log(buffer);
}
