#!/usr/bin/env bash
cd $(dirname $(dirname $0))
exec node lib/cli.js "$@"
